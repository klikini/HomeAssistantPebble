/**
 * Make a request to the Home Assistant instance.
 * @param {"GET"|"POST"} method HTTP method to use
 * @param {String} endpoint API endpoint to call
 * @param {function(String): void} callback Called on success (2xx) with response text
 * @param {Object} data Data to send as JSON
 */
function request(method, endpoint, callback, data = null) {
	const req = new XMLHttpRequest();
	const url = localStorage["ha_instance"] + "api/" + endpoint;

	req.addEventListener("loadend", function () {
		if (Math.floor(req.status / 100) !== 2) {
			console.log(method + " " + url + " - "
				+ (req.status ? ("server responded with: " + req.status + " " + req.statusText) : "XHR error"));
		} else {
			// Request succeeded and reply was 2xx
			callback(JSON.parse(req.response));
		}
	});

	req.timeout = 2000; // 2 seconds
	req.open(method, url);
	req.setRequestHeader("Authorization", "Bearer " + localStorage["ha_token"]);
	req.setRequestHeader("Content-Type", "application/json");
	req.send(data ? JSON.stringify(data) : null);
}

function sendEntities() {
	const entities = JSON.parse(localStorage["entities"] || "[]");

	Pebble.sendAppMessage({
		"RESET": entities.length
	}, function () {
		for (const entity of entities) {
			const [id, name] = entity.split("|", 2);
			request("GET", `states/${id}`, function (data) {
				Pebble.sendAppMessage({
					"ENTITY_ID": id,
					"ENTITY_NAME": name,
					"ENTITY_STATE": data["state"],
				}, function () {
					console.log("Entity '" + entity + "' sent to watch");
				}, function (error) {
					console.log("Error sending config entity '" + entity + "' to watch: " + error.type);
				});
			});
		}
	}, function (error) {
		console.log("Error starting entity transmission: " + error.type);
	});
}

Pebble.addEventListener("ready", function () {
	console.log("PebbleKit JS ready");
	sendEntities();
});

Pebble.addEventListener("appmessage", function (message) {
	let data = message.payload;

	if (data["RESET"]) {
		sendEntities();
	} else if (data["ENTITY_ID"] && data["ENTITY_STATE"]) {
		// Set entity state
		const domain = data["ENTITY_ID"].split(".")[0];

		request("POST", "services/" + domain + "/turn_" + data["ENTITY_STATE"], function (response) {
			if (response["state"]) {
				console.log("Set entity '" + data["ENTITY_ID"] + "' state: " + JSON.stringify(response));
				Pebble.sendAppMessage({
					"ENTITY_ID": data["ENTITY_ID"],
					"ENTITY_STATE": response["state"]
				}, function () {
					console.log("Sent updated entity '" + data["ENTITY_ID"] + "' to watch");
				}, function (error) {
					console.log("Error sending updated entity '" + data["ENTITY_ID"] + "' to watch: " + error.type);
				});
			} else {
				console.log("Error updating entity '" + data["ENTITY_ID"] + "': " + JSON.stringify(response));
			}
		}, {
			"entity_id": data["ENTITY_ID"],
		});
	}
});

Pebble.addEventListener("showConfiguration", function () {
	Pebble.openURL("https://klikini.gitlab.io/HomeAssistantPebble/");
});

Pebble.addEventListener("webviewclosed", function (event) {
	if (event.response) {
		const args = new URLSearchParams(event.response);
		localStorage["ha_instance"] = args.get("instance");
		localStorage["ha_token"] = args.get("token");
		localStorage["entities"] = JSON.stringify(args.getAll("entity"));
	} else {
		console.log("No config data received");
	}

	sendEntities();
});
