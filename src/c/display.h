#ifndef DISPLAY_H
#define DISPLAY_H

#include <pebble.h>
#include "entity.h"

#define PADDING 15

static const uint32_t const prv_vs_switch_flip[] = { 20 };
static VibePattern prv_switch_flip = {
    .durations = prv_vs_switch_flip,
    .num_segments = 1,
};

static Window* s_window;
static StatusBarLayer* s_status_bar;
static GBitmap* g_blank;

// Main entity display
static struct haEntity* s_current = NULL;
static bool s_current_on = false;
static TextLayer* s_text_current, * s_text_state;
static BitmapLayer* s_state_canvas;
static GBitmap* g_switch_on, * g_switch_off;

// Button labels
static ActionBarLayer* s_action_bar;
static GBitmap* g_icon_up, * g_icon_down, * g_icon_turn_on, * g_icon_turn_off;

void display_entity(struct haEntity* entity) {
    if (!entity) {
        return;
    }

    s_current = entity;
    s_current_on = !strcmp(s_current->state, "on");

    text_layer_set_text(s_text_current, s_current->name);
    text_layer_set_text(s_text_state, s_current->state);
    bitmap_layer_set_bitmap(s_state_canvas, s_current_on ? g_switch_on : g_switch_off);

    action_bar_layer_set_icon(s_action_bar, BUTTON_ID_UP, s_current->prev ? g_icon_up : g_blank);
    action_bar_layer_set_icon(s_action_bar, BUTTON_ID_SELECT, s_current_on ? g_icon_turn_off : g_icon_turn_on);
    action_bar_layer_set_icon(s_action_bar, BUTTON_ID_DOWN, s_current->next ? g_icon_down : g_blank);
}

static void prv_select_click_handler(ClickRecognizerRef recognizer, void* context) {
    if (s_current && send_entity_state(s_current, s_current_on ? "off" : "on")) {
        display_entity(s_current);
        vibes_enqueue_custom_pattern(prv_switch_flip);
    }
}

static void prv_up_click_handler(ClickRecognizerRef recognizer, void* context) {
    if (s_current) {
        display_entity(s_current->prev);
    }
}

static void prv_down_click_handler(ClickRecognizerRef recognizer, void* context) {
    if (s_current) {
        display_entity(s_current->next);
    }
}

static void prv_click_config_provider(void* context) {
    window_single_click_subscribe(BUTTON_ID_SELECT, prv_select_click_handler);
    window_single_click_subscribe(BUTTON_ID_UP, prv_up_click_handler);
    window_single_click_subscribe(BUTTON_ID_DOWN, prv_down_click_handler);
}

static void prv_window_load(Window* window) {
    g_blank = gbitmap_create_blank(GSizeZero, GBitmapFormat1Bit);
    g_icon_up = gbitmap_create_with_resource(RESOURCE_ID_ICON_UP);
    g_icon_down = gbitmap_create_with_resource(RESOURCE_ID_ICON_DOWN);
    g_icon_turn_on = gbitmap_create_with_resource(RESOURCE_ID_ICON_TURN_ON);
    g_icon_turn_off = gbitmap_create_with_resource(RESOURCE_ID_ICON_TURN_OFF);

    Layer* window_layer = window_get_root_layer(window);
    GRect bounds = layer_get_bounds(window_layer);

    s_status_bar = status_bar_layer_create();
    status_bar_layer_set_colors(s_status_bar, GColorBlack, GColorWhite);
    layer_add_child(window_layer, status_bar_layer_get_layer(s_status_bar));

    s_text_current = text_layer_create(GRect(PADDING, PADDING + STATUS_BAR_LAYER_HEIGHT, bounds.size.w - ACTION_BAR_WIDTH - 2 * PADDING, 40));
    layer_add_child(window_layer, text_layer_get_layer(s_text_current));

    s_text_state = text_layer_create(GRect(PADDING, bounds.size.h - PADDING - 40, bounds.size.w - ACTION_BAR_WIDTH - 2 * PADDING, 40));
    layer_add_child(window_layer, text_layer_get_layer(s_text_state));

    s_state_canvas = bitmap_layer_create(GRect(PADDING, bounds.size.h / 2 + STATUS_BAR_LAYER_HEIGHT / 2 - 15, bounds.size.w - ACTION_BAR_WIDTH - 2 * PADDING, 30));
    bitmap_layer_set_alignment(s_state_canvas, GAlignLeft);
    bitmap_layer_set_compositing_mode(s_state_canvas, GCompOpSet);
    layer_add_child(window_layer, bitmap_layer_get_layer(s_state_canvas));

    s_action_bar = action_bar_layer_create();
    action_bar_layer_add_to_window(s_action_bar, window);
    action_bar_layer_set_click_config_provider(s_action_bar, prv_click_config_provider);

    g_switch_off = gbitmap_create_with_resource(RESOURCE_ID_SWITCH_OFF);
    g_switch_on = gbitmap_create_with_resource(RESOURCE_ID_SWITCH_ON);
}

static void prv_window_unload(Window* window) {
    text_layer_destroy(s_text_current);
    text_layer_destroy(s_text_state);
    action_bar_layer_destroy(s_action_bar);
    bitmap_layer_destroy(s_state_canvas);
}

void display_init() {
    s_window = window_create();
    window_set_window_handlers(s_window, (WindowHandlers) {
        .load = prv_window_load,
            .unload = prv_window_unload,
    });

    window_stack_push(s_window, (const bool)true);
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Done initializing, pushed window: %p", s_window);
}

void display_deinit() {
    gbitmap_destroy(g_blank);
    gbitmap_destroy(g_icon_up);
    gbitmap_destroy(g_icon_down);
    gbitmap_destroy(g_icon_turn_on);
    gbitmap_destroy(g_icon_turn_off);
    gbitmap_destroy(g_switch_on);
    gbitmap_destroy(g_switch_off);
    status_bar_layer_destroy(s_status_bar);
    window_destroy(s_window);
}

#endif