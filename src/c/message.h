#ifndef MESSAGE_H
#define MESSAGE_H

static void prv_inbox_received_callback(DictionaryIterator* iter, void* context) {
    static uint8_t expected_count = 0;

    Tuple* new_count = dict_find(iter, MESSAGE_KEY_RESET);

    if (new_count) {
        clear_entities();
        expected_count = new_count->value->uint8;
        return;
    }

    Tuple* entity_id = dict_find(iter, MESSAGE_KEY_ENTITY_ID);
    Tuple* entity_name = dict_find(iter, MESSAGE_KEY_ENTITY_NAME);
    Tuple* entity_state = dict_find(iter, MESSAGE_KEY_ENTITY_STATE);

    if (entity_id && entity_name) {
        add_entity(entity_id->value->cstring,
            entity_name->value->cstring,
            entity_state ? entity_state->value->cstring : NULL);

        if (entity_count == expected_count) {
            display_entity(entities);
        }
    } else if (entity_id && entity_state) {
        if (set_entity_state(entity_id->value->cstring, entity_state->value->cstring) == s_current) {
            display_entity(s_current);
        }
    }
}

void comm_init() {
    app_message_register_inbox_received(prv_inbox_received_callback);
    app_message_open(ENTITY_ID_LEN + ENTITY_NAME_LEN, ENTITY_ID_LEN);
}

#endif
