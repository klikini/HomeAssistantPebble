#ifndef ENTITY_H
#define ENTITY_H

#define ENTITY_ID_LEN 60
#define ENTITY_NAME_LEN 40
#define ENTITY_STATE_LEN 20

uint8_t entity_count = 0;

struct haEntity {
    // Common information
    char id[ENTITY_ID_LEN + 1];
    char name[ENTITY_NAME_LEN + 1];
    char state[ENTITY_STATE_LEN + 1];

    // Linked list
    struct haEntity* prev;
    struct haEntity* next;
};

struct haEntity* entities = NULL;

void add_entity(char* id, char* name, char* state) {
    static struct haEntity* ll_end = NULL;
    struct haEntity* entity = malloc(sizeof(struct haEntity));

    if (!entity) {
        window_stack_pop_all(false);
    }

    strncpy(entity->id, id, ENTITY_ID_LEN);
    strncpy(entity->name, name, ENTITY_NAME_LEN);

    if (state) {
        strncpy(entity->state, state, ENTITY_STATE_LEN);
    }

    entity->prev = NULL;
    entity->next = NULL;

    if (entities) {
        // Subsequent entity
        ll_end->next = entity;
        entity->prev = ll_end;
    } else {
        // First entity
        entities = entity;
        ll_end = entity;
    }

    ll_end = entity;
    entity_count++;
}

bool send_entity_state(struct haEntity* entity, char* state) {
    // Send to PKJS
    DictionaryIterator* out_iter;
    AppMessageResult result = app_message_outbox_begin(&out_iter);

    if (result != APP_MSG_OK) {
        APP_LOG(APP_LOG_LEVEL_ERROR, "Error preparing outbox: %d", result);
        return false;
    }

    dict_write_cstring(out_iter, MESSAGE_KEY_ENTITY_ID, entity->id);
    dict_write_cstring(out_iter, MESSAGE_KEY_ENTITY_STATE, state);

    result = app_message_outbox_send();

    if (result != APP_MSG_OK) {
        APP_LOG(APP_LOG_LEVEL_ERROR, "Error sending outbox: %d", result);
        return false;
    }

    // Update local state
    strncpy(entity->state, state, ENTITY_STATE_LEN);
    return true;
}

struct haEntity* set_entity_state(char* entity_id, char* state) {
    struct haEntity* entity = entities;

    while (strcmp(entity->id, entity_id)) {
        if (!entity) {
            return NULL;
        }

        entity = entity->next;
    }

    strncpy(entity->state, state, ENTITY_STATE_LEN);
    return entity;
}

void clear_entities() {
    struct haEntity* last;

    while (entities) {
        last = entities;
        entities = entities->next;
        free(last);
    }

    last = NULL;
    entity_count = 0;
}

void entity_deinit() {
    clear_entities();
}

#endif