#include <pebble.h>

#include "entity.h"
#include "display.h"
#include "message.h"

int main(void) {
	// Startup
	comm_init();
	display_init();

	// Run
	app_event_loop();

	// Shutdown
	display_deinit();
	entity_deinit();
}
