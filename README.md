# Home Assistant app for Pebble watches

![Screenshot](screenshot.png)

## Installing

1. Download the latest [HomeAssitantPebble.pbw](https://gitlab.com/klikini/HomeAssistantPebble/-/jobs/artifacts/main/raw/build/HomeAssistantPebble.pbw?job=pbw) on your mobile device
1. Open the file with the Pebble app to install it. On newer versions of Android, you might need to install [Sideload Helper](https://help.rebble.io/sideloading/) for this to work.

## Contributing

1. [Install the Pebble command line tools](https://developer.rebble.io/developer.pebble.com/sdk/install/index.html)
1. You may need to run `alias python='python2.7'` during setup (and add it to `.env/bin/activate` afterwards).
1. Once the `pebble` tool is installed, make sure you have npm installed and then run `pebble sdk install https://github.com/aveao/PebbleArchive/raw/master/SDKCores/sdk-core-4.3.tar.bz2` to install the SDK.
